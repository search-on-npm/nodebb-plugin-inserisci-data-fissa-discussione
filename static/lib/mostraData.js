"use strict";

(function() {
	$(window).on('action:topic.tools.load', function(event, data) {
		$('.thread-tools [component="topic/pin"]').on('click', function(ev) {

			ajaxify.loadTemplate('modals/connect-data-fissa-discussione', function(dataTopicRilievo) {

				var modal = bootbox.dialog({
					message: dataTopicRilievo,
					title: 'Selezionare data topic in rilievo',
					buttons: {
						success: {
							label: "Save",
							className: "btn-primary save",
							callback: function() {
								var per_sempre_boolean = $('#per_sempre');

								var timestamp_iniziale = 0;
								var timestamp_finale = 0;
								var tid = ajaxify.data.tid;
								if (!per_sempre_boolean.is(':checked')) {
									var timestamp_iniziale_input = $('#topic_giorno_iniziale').val();
									var timestamp_finale_input = $('#topic_giorno_finale').val();
									if (timestamp_iniziale_input && timestamp_finale_input) {
										timestamp_iniziale = (new Date(timestamp_iniziale_input)).getTime();
										//aggiungo 86400 che sarebbe 24*60 minuti*60 secondi perchè teoricamente dovrebbe finire
										//all'inizio del giorno
										timestamp_finale = (new Date(timestamp_finale_input)).getTime() + 86400000;

										if (timestamp_iniziale > timestamp_finale) {
											app.alertError("La data iniziale deve essere ANTECEDENTE alla data finale");
											return false;
										} else {
											if (timestamp_iniziale == (timestamp_finale - 86400000)) {
												app.alertError("Data iniziale e finale devono essere diverse");
												return false;
											}
										}

										//console.log("DATA INIZIALE "+JSON.stringify($('#topic_giorno_iniziale').val()));
									} else {
										if (!timestamp_iniziale_input) {
											app.alertError('Data iniziale non inserita correttamente');

										}
										if (!timestamp_finale_input) {
											app.alertError("Data finale non inserita correttamente");

										}
										return false;
									}
									//socket.emit('plugins.connectinseriscidatafissadiscussione.salva_date_rilievo_topic',{'timestamp_iniziale':timestamp_iniziale,'timestamp_finale':timestamp_finale,'tid':tid})
								}
								socket.emit('plugins.connectinseriscidatafissadiscussione.salva_date_rilievo_topic', {
									'timestamp_iniziale': timestamp_iniziale,
									'timestamp_finale': (timestamp_finale - 86400000),
									'tid': tid
								}, function(err) {
									if (err) {

										app.alertError(err);
									}
								});
							}
						}
					}
				});
				modal.on('shown.bs.modal', function() {
					$('#topic_giorno_iniziale').val(modificaData(new Date()));
					socket.emit('plugins.connectinseriscidatafissadiscussione.getDateRilievoTopic', {
						tid: ajaxify.data.tid
					}, function(err, fissaTopic) {
						if (err) {
							app.alertError('Error loading data, try again');
						}
						if (fissaTopic) {
							if (fissaTopic.timestamp_iniziale == 0) {
								$('#per_sempre').prop('checked', true);

							} else {
								var date = new Date(fissaTopic.timestamp_iniziale);
								var testo_tempo_iniziale = modificaData(date);
								date = new Date(fissaTopic.timestamp_finale);
								var testo_tempo_finale = modificaData(date);

								$('#topic_giorno_iniziale').val(testo_tempo_iniziale);
								$('#topic_giorno_finale').val(testo_tempo_finale);
							}
						}
					});

				});
				modal.on('click', '[data-dismiss="modal"]', function() {

					//console.log("DIMISS");
					socket.emit('plugins.connectinseriscidatafissadiscussione.removePinnedTopic', {
						tid: ajaxify.data.tid
					}, function(err) {
						if (err) {
							app.alertError(err);
						}


					});

				});


			});
			ev.preventDefault();
		});
	});

	function modificaData(date) {
		var mm = date.getMonth() + 1; // getMonth() is zero-based
		var dd = date.getDate();
		var mese = "";
		var day = "";
		if (mm < 10) {
			mese = "0" + mm;
		} else {
			mese = "" + mm;
		}
		if (dd < 10) {
			day = "0" + dd;
		} else {
			day = "" + dd;
		}
		return date.getFullYear() + "-" + mese + "-" + day;

	}


}());