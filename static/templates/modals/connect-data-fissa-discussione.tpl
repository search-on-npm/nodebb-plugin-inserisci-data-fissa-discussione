<div>
	<h4>Selezionare l'intervallo temporale entro il quale questo topic dovrà essere in RILIEVO </h4>
	<br>
	<table width="100%">
		<tbody>
		<tr>
			<td>
				<div style="text-align:center">
					<label>Giorno Iniziale</label><br/>
    				<input type="date" id="topic_giorno_iniziale">
    			</div>
    		</td>
    		<td>
    			<div style="text-align:center">
    				<label>Giorno Finale</label><br/>
    				<input type="date" id="topic_giorno_finale">
    			</div>
    		</td>
		</tr>
			
		</tbody>
	</table>
	<br>
	<br>
	<div style="text-align:center" width="100%">
		<input type="checkbox" id="per_sempre" /> Selezionare questo campo se si ritiene che la discussione dovrà essere in rilievo per sempre
	</div>

</div>
