"use strict";

var converter = {};
var user = require.main.require('./src/user');
var db = require.main.require('./src/database'),
	plugins = require.main.require('./src/plugins'),
	async = require.main.require('async'),
	winston = require.main.require('winston'),
	topics = require.main.require('./src/topics'),
	sockets = require('./lib/sockets'),
	SocketPlugins = require.main.require('./src/socket.io/plugins'),
	categories = require.main.require('./src/categories');
var click_zero = false;
var hash = "connect-inserisci-data-fissa-discussione:";
var discussione = {};

SocketPlugins.connectinseriscidatafissadiscussione = sockets;


/*discussione.checkTopicPinned = function(data, callback) {

	if (!data || !data.topics || data.topics.length == 0) {
		return callback(null, data);
	}
	var lista_topics = data.topics;
	async.each(lista_topics, function(topic, prossima) {
		//console.log("TOPIC PINNED " + topic.tid + " " + topic.pinned);
		if (!topic.pinned) {
			return prossima();
		}
		var tid = topic.tid;
		async.waterfall([
			function(next) {
				db.getObject(hash + tid, next);
			},
			function(intervalloTemporale, next) {
				if (!intervalloTemporale) {
					return prossima();
				}

				var timestamp_finale = intervalloTemporale.timestamp_finale;
				var timestamCorrente = Date.now();
				//console.log("TIMESTAMP FINALE " + tid + " TImestamp " + timestamp_finale);
				if (timestamCorrente < timestamp_finale) {
					return prossima();
				} else {
					return next();
				}

			},
			//va qui solo se il tempo corrente è maggiore del tempo finale(tempo scadenza)
			function(next) {
				topics.setTopicField(tid, ['pinned'], false, next);
			},
			function(next) {
				db.delete(hash + tid, next);
			}

		], function(err) {
			if (err) {
				return callback(err);
			}
			return prossima();
		});

		//console.log("TOPIC " + JSON.stringify(topic));
	}, function(err) {
		if (err) {
			return callback(err);
		}
		return callback(null, data);
	});

};*/



module.exports = discussione;