'use strict';

var db = require.main.require('./src/database'),
	winston = require.main.require('winston'),
	websockets = require.main.require('./src/socket.io'),
	async = require.main.require('async'),
	user = require.main.require('./src/user'),
	topics = require.main.require('./src/topics'),
	utils = require.main.require('./public/src/utils');

var hash = "connect-inserisci-data-fissa-discussione:";

var Sockets = {};

//Permette il salvataggio della data iniziale,data finale e dell'id del topic
Sockets.salva_date_rilievo_topic = function(socket, data, callback) {
	if (!data) {
		return callback(new Error("Parametri errati in inserisci data fissa discussione"));
	}
	var timestamp_iniziale = data.timestamp_iniziale;
	var timestamp_finale = data.timestamp_finale;
	var topic_id = data.tid;
	var oggetto = {
		'timestamp_iniziale': timestamp_iniziale,
		'timestamp_finale': timestamp_finale,
		'tid': topic_id
	};
	db.setObject(hash + topic_id, oggetto, callback);
	return callback(null, true);

};
//Ritorna l'intervallo temporale di un topic
Sockets.getDateRilievoTopic = function(socket, data, callback) {
	db.getObject(hash + data.tid, function(err, fissaTopic) {
		if (err) {
			return callback(err);
		}
		return callback(null, fissaTopic);
	});
};

//Modifica il pinned di un topic
Sockets.removePinnedTopic = function(socket, data, callback) {
	var tid = data.tid;
	async.waterfall([
		function(next) {
			db.getObject(hash + tid, next);
		},
		function(dataInDatabase, next) {
			//console.log("REMOVE PINNED "+JSON.stringify(dataInDatabase));
			if (dataInDatabase == null || dataInDatabase == undefined) {
				topics.setTopicField(data.tid, ['pinned'], false, next);
			} else {
				next();
			}

		}
	], function(err) {
		if (err) {
			return callback(err);
		}
		return callback();
	});


};

module.exports = Sockets;